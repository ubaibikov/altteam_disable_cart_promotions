<?php

/*****************************************************************************
 * This is a commercial software, only users who have purchased a  valid
 * license and accepts the terms of the License Agreement can install and use  
 * this program.
 *----------------------------------------------------------------------------
 * @copyright  LCC Alt-team: http://www.alt-team.com
 * @license    http://www.alt-team.com/addons-license-agreement.html
 ****************************************************************************/

use Tygh\Registry;

if (!defined('BOOTSTRAP')) {
    die('Access denied');
}

function fn_altteam_disable_cart_promotions_calculate_cart(&$cart, &$cart_products, $auth, $calculate_shipping, $calculate_taxes, $apply_cart_promotions)
{

    $cart['total'] = $cart['display_subtotal'];
    $cart['total'] += $cart['tax_subtotal'];
    $cart['total'] = fn_format_price($cart['total'] + $cart['shipping_cost']);

    $cart['subtotal_discount'] = 0;
    $price_without_promotion = 0;

    foreach ($cart_products as $product) {
        if (empty($product['promotions'])) {
            $price_without_promotion += $product['price'] * $product['amount'];
        }
    }

    foreach ($cart['applied_promotions'] as $applied_promotion) {
        foreach ($applied_promotion['bonuses'] as $bonus) {
            if ($price_without_promotion > 0) {
                $discount = fn_promotions_calculate_discount($bonus['discount_bonus'], $price_without_promotion, $bonus['discount_value']);
                if (floatval($discount)) {
                    $cart['subtotal_discount'] += fn_format_price($discount);
                }
            }
        }
    }

    if (!empty($cart['subtotal_discount'])) {
        $cart['discounted_subtotal'] = $cart['subtotal'] - ($cart['subtotal_discount'] < $cart['subtotal'] ? $cart['subtotal_discount'] : $cart['subtotal']);
        $cart['total'] -= ($cart['subtotal_discount'] < $cart['total']) ? $cart['subtotal_discount'] : $cart['total'];
    }
}
